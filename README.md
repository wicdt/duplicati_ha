## docker-duplicati

Fork of [https://github.com/hydazz/docker-duplicati](https://github.com/hydazz/docker-duplicati)


[Duplicati](https://www.duplicati.com/) is a free backup software to store encrypted backups online, Duplicati works with standard protocols like FTP, SSH, WebDAV as well as popular services like Microsoft OneDrive, Amazon Cloud Drive and S3, Google Drive, box.com, Mega, hubiC and many others.

## Usage


**Read the official [README.md](https://github.com/linuxserver/docker-duplicati/) for more information**

## Upgrading Duplicati

To upgrade, all you have to do is pull the latest Docker image. We automatically check for Duplicati updates daily. When a new version is released, we build and publish an image both as a version tag and on `:latest`.

## Fixing Appdata Permissions

If you ever accidentally screw up the permissions on the appdata folder, run `fix-perms` within the container. This will restore most of the files/folders with the correct permissions.
