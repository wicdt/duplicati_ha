ARG BUILD_FROM
FROM $BUILD_FROM

# set version label
ARG VERSION

# environment settings
ENV LANG de_DE.UTF-8
ENV HOME="/config/duplicati"

RUN set -xe && \
	echo "**** install packages ****" && \
	apk add --no-cache -X http://dl-cdn.alpinelinux.org/alpine/edge/testing \
		ca-certificates \
		mono \
		jq \
		libgdiplus \
		sqlite-libs \
		terminus-font && \
	echo "**** install duplicati ****" && \
	mkdir -p /app/duplicati && \
	if [ -z ${VERSION} ]; then \
		VERSION=$(curl -sL "https://api.github.com/repos/duplicati/duplicati/releases" | \
			jq -r 'first(.[] | select(.tag_name | contains("beta"))) | .tag_name'); \
	fi && \
	curl -o \
		/tmp/duplicati.zip -L \
		"$(curl -s https://api.github.com/repos/duplicati/duplicati/releases/tags/${VERSION} | \
			jq -r '.assets[].browser_download_url' | grep zip | grep -v signatures)" && \
	unzip \
		/tmp/duplicati.zip -d \
		/app/duplicati && \
	echo "**** cleanup ****" && \
	rm -rf \
		/tmp/*

# copy local files
COPY root/ /